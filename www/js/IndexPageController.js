/*jslint browser: true*/
/*global console*/

var myapp = myapp || {};
myapp.pages = myapp.pages || {};

var IMAGENS = new imagens();

myapp.pages.IndexPageController = function (myapp, $$) {
  'use strict';

  // Init method
  (function () {
    var options = {
      'bgcolor': '#990033',
      'fontcolor': '#fff',
      // parallax: true|false,
      // parallaxBackgroundImage: 'http://lorempixel.com/900/600/nightlife/2/', // parallax default background image
      // parallaxBackground: '-23%', // parallax default background effect
      /* parallaxSlideElements: {
            title: -100,
            subtitle: -300,
            text: -500
        }, */
      'onOpened': function () {
        console.log("welcome screen opened");
      },
      'onClosed': function () {
        console.log("welcome screen closed");
		// start tour
		tourguide.showTour();
      }
    },
    welcomescreen_slides = [
      {
        id: 'slide0',
        title: '<h3 style="font-size:10px;">SEJA BEM VINDO AO NOVO</h3><B style="font-size:20px;">AÇAÍ DELIVERY BELÉM </b>',
        picture: '<div class="tutorialicon"><img src="'+IMAGENS.garoto()+'" width="80%" height="80%"/></div>',
        text: '<h3 style="font-size:12px;">Eu sou o Açaízinho e vou estar com você acompanhando por todo o aplicativo! Agora. Vou te contar um pouco da minha historia...</h3>'
      },
      {
        id: 'slide1',
        title: '<b style="font-size:22px;">Começo do Acaí Delivery</b><h3 style="font-size:12px;">Tudo começou na Unama com meus dois tios: Daniel e Valdson. Eles tomaram um açaí mágico. Que deu superpoderes para eles. Tiveram uma super  ideia em criar um aplicativo de entrega de açaí. ',
        picture: '<div class="tutorialicon" style="margin-top:calc(100% - 80%) ;"><img src="'+IMAGENS.danielevaldson()+'" width="100%" height="80%"/></div>',
        //text: ''
      },
      {
        id: 'slide2',
        title: '<b style="font-size:22px;">SEBRAE-PA</b><h3 style="font-size:12px;">Eles voaram para o SEBRAE. E lá eles falaram com tio Renato. Que tem superpoderes empreendedores. Ele nos ensinou a como desenvolver a nossa ideia. Lutou e batalhou ao nosso lado, como um orientador. Puxou de sua mochila uma ferramenta mágica chamada CANVAS, que nos ajuda a trabalhar as nossas ideias. </h3>',
        picture: '<div class="tutorialicon" style="margin-top:-30px;"><img src="'+IMAGENS.renato()+'" width="80%" height="80%"/></div>',
        //text: ''
      },
      {
        id: 'slide3',
        title: '<b style="font-size:22px;">UNAMA</b><h3 style="font-size:12px;">Voamos de lá para a UNAMA quando falamos com o mago tio Rômulo. Ele nos ajudou apresentando alguns feiticeiros professores que nos ensinaram muitas mágicas para desenvolver nosso super aplicativo .  </h3>',
        picture: '<div class="tutorialicon"style="margin-top:-30px;"><img src="'+IMAGENS.romulo()+'" width="80%" height="80%"/></div>',
       // text: 'Iai vamos tomar esse açaí juntos?<br><br><a class="tutorial-close-btn" href="#">Finalizar Introdução</a>'
      },
      {
        id: 'slide4',
        title: '<b style="font-size:22px;">CODES-UNAMA</b><h3 style="font-size:12px;">E depois de desenvolver o super aplicativo, construímos um navio na UNAMA chamada CODES(comunidade de desenvolvimento de startup da UNAMA), convidamos alguns marinheiros para se juntar a nossa tripulação e junto navegamos em direção ao sucesso. </h3>',
        picture: '<div class="tutorialicon"style="margin-top:-30px;"><img src="'+IMAGENS.danielzinho()+'" width="80%" height="80%"/></div>',
       // text: 'Iai vamos tomar esse açaí juntos?<br><br><a class="tutorial-close-btn" href="#">Finalizar Introdução</a>'
      },{
        id: 'slide5',
        title: '<b style="font-size:20px;">Selo de qualidade</b><h3 style="font-size:10px;">Temos o compromisso com a sua saúde e a qualidade do produto que chega até você! Todos nossos batedores são legalizados pelo município!</h3>',
        picture: '<div class="tutorialicon"style="margin-top:-30px;"><img src="'+IMAGENS.seloacaibom()+'" width="80%" height="80%"/></div>',
        //text: '<h3 style="font-size:12px;">Eu sou o Açaízinho e vou estar com você acompanhando por todo o aplicativo! Agora. Vou te contar um pouco da minha historia...</h3>'
      },{
        id: 'slide5',
        title: '<b style="font-size:20px;">Obrigado por estar conosco!</b><h3 style="font-size:10px;">Vamos tomar um açaí?</h3>',
        picture: '<div class="tutorialicon"style="margin-top:-30px;"><img src="'+IMAGENS.garoto()+'" width="80%" height="80%"/></div>',
        text: 'Clique em finalizar para iniciar o aplicativo!	<br><br><a class="tutorial-close-btn" onclick="tuto();" href="#">Finalizar Introdução</a>'
      }
    ],
    welcomescreen = myapp.welcomescreen(welcomescreen_slides, options);

    $$(document).on('click', '.tutorial-close-btn', function () {
      welcomescreen.close();
    });

    $$('.tutorial-open-btn').click(function () {
      welcomescreen.open();
    });

    $$(document).on('click', '.tutorial-next-link', function (e) {
      welcomescreen.next();
    });

    $$(document).on('click', '.tutorial-previous-slide', function (e) {
      welcomescreen.previous();
    });

  }());

};
