var myApp = new Framework7({
      pushState: true,
      swipePanel: 'left'
    });

    $$ = Dom7;
		/* Initialize views */
    var mainView = myApp.addView('.view-main', {
        dynamicNavbar: true
      });
    initTourGuide = function (){
      var tourSteps = [],
            options = {previousButton: true};
      tourSteps.push({
        step: 0,
        header: 'Política de Privacidade',
        message: 'Esse botão mostra pra você o documento no qual você estará aceitando os termos de compromisso de armazenamento de dados e de utilização de informações.',
        element: "body > div.views > div > div.pages > div > div > div > center > div.list-block > p:nth-child(1) > a:nth-child(1)",
        action: function ()
        {
            console.log('Started guided tour');
            console.log('Step 0');
        }
      });
      tourSteps.push({
        step: 1,
        header: 'Permissão de utilização de termos (Assinatura do usuário)',
        message: 'Ao aceitar nesse campo você confirma com nossos termos de uso e utilização de informações e dados.',
        element: "body > div.views > div > div.pages > div > div > div > center > div.list-block >  ul > li:nth-child(1)",
        action: function ()
        {
            console.log('Step 1');
        }
      });
      tourSteps.push({
        step: 2,
        header: 'Tema',
        message: 'Nesse campo você define o tema do aplicativo! se ele vai ser claro ou escuro.',
        element: "body > div.views > div > div.pages > div > div > div > center > div.prfa >  ul > li:nth-child(1)",
        action: function ()
        {
            console.log('Step 2');
        }
      });
      tourSteps.push({
        step: 3,
        header: 'Concluir Introdução',
        message: 'Botão no qual você armazena todas as informações desse formulário preenchido.',
        element: "body > div.views > div > div.pages > div > div > div > center > p > a:nth-child(1)",
        action: function ()
        {
            console.log('Step 3');
        }
      });
      tourguide = myApp.tourguide(tourSteps, options);
    };