
/* Verifica email */
//Inicio
function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function validate() {
  var email = $("#inputemail").val();
  if (validateEmail(email)) {
		document.getElementById('emailvalidation').innerHTML = '<img src="img/uservalido.png" width="20px" height="20px" />';

	}else{
		document.getElementById('emailvalidation').innerHTML = '<img src="img/userinvalido.png" width="20px" height="20px" />';

	}
  return false;
}
//Fim
/* Verifica idade */
// Inicio
function idade(ano_aniversario, mes_aniversario, dia_aniversario) {
    var d = new Date,
        ano_atual = d.getFullYear(),
        mes_atual = d.getMonth() + 1,
        dia_atual = d.getDate(),

        ano_aniversario = +ano_aniversario,
        mes_aniversario = +mes_aniversario,
        dia_aniversario = +dia_aniversario,

        quantos_anos = ano_atual - ano_aniversario;

    if (mes_atual < mes_aniversario || mes_atual == mes_aniversario && dia_atual < dia_aniversario) {
        quantos_anos--;
    }

    return quantos_anos < 0 ? 0 : quantos_anos;
}



function verificaidade(data,id){
	data = data.split('-');
	console.log(idade(data[0],data[1], data[2]));
	if (idade(data[0],data[1], data[2]) >= 18){
	 console.log('entrou');
		document.getElementById(id).innerHTML = '<img src="img/uservalido.png" width="20px" height="20px" />';

	}else{
	 console.log('saiu');
		document.getElementById(id).innerHTML = '<img src="img/userinvalido.png" width="20px" height="20px" />';

	}
}
//Fim
/* Verificação se campo está vazio */
// Inicio
function verificainputvazio(data,id){
	if(data == ''){
		document.getElementById(id).innerHTML = '<img src="img/userinvalido.png" width="20px" height="20px" />';

	}else{
		document.getElementById(id).innerHTML = '<img src="img/uservalido.png" width="20px" height="20px" />';

	}
}
// Fim
/* Mascará para cpf */
//inicio
function fMasc(objeto,mascara) {
				obj=objeto;
				masc=mascara;
				setTimeout("fMascEx()",1);
			}
			function fMascEx() {
				obj.value=masc(obj.value);
			}
function  mCPF(cpf){
				cpf=cpf.replace(/\D/g,"");
				cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2");
				cpf=cpf.replace(/(\d{3})(\d)/,"$1.$2");
				cpf=cpf.replace(/(\d{3})(\d{1,2})$/,"$1-$2");
				return cpf;
			}
// Fim
/* Mascará de telefone */
//inicio
function mascara(o,f){
    v_obj=o;
    v_fun=f;
    setTimeout("execmascara()",1);
}
function execmascara(){
    v_obj.value=v_fun(v_obj.value);
}
function mtel(v){
    v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
    v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
    v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
    return v;
}
function id( el ){
	return document.getElementById( el );
}
window.onload = function(){
	id('inputtelefone').onkeyup = function(){
		mascara( this, mtel );
		verificainputvazio(this.value,'telefonevalidation');
	}
}
// FIm