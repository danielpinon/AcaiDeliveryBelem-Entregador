
// Verifica telefone
function mascara(o,f){
    v_obj=o;
    v_fun=f;
    setTimeout("execmascara()",1);
}
function execmascara(){
    v_obj.value=v_fun(v_obj.value);
}
function mtel(v){
    v=v.replace(/\D/g,"");             //Remove tudo o que não é dígito
    v=v.replace(/^(\d{2})(\d)/g,"($1) $2"); //Coloca parênteses em volta dos dois primeiros dígitos
    v=v.replace(/(\d)(\d{4})$/,"$1-$2");    //Coloca hífen entre o quarto e o quinto dígitos
    return v;
}
function id( el ){
	return document.getElementById( el );
}
window.onload = function(){
	id('telefone').onkeyup = function(){
		mascara( this, mtel );
		verificainputvazio(this.value,'telefonevalidation');
	}
}

// Verifica email
function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function validate() {
  var email = $("#inputemail").val();
  if (validateEmail(email)) {
		document.getElementById('validation').innerHTML = '<img src="img/uservalido.png" width="20px" height="20px" />';
	}else{
		document.getElementById('validation').innerHTML = '<img src="img/userinvalido.png" width="20px" height="20px" />';
	}
  return false;
}
// verifica campo vazio
function verificainputvazio(data,id){
	if(data == ''){
		document.getElementById(id).innerHTML = '<img src="img/userinvalido.png" width="20px" height="20px" />';
	}else{
		document.getElementById(id).innerHTML = '<img src="img/uservalido.png" width="20px" height="20px" />';
	}
}