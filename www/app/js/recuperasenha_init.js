
	function voltar(){
		 myApp.confim('Deseja realmente voltar?',function(){
			window.location.href='login.html';
		 });
	}

	function recuperar(){
		myApp.showIndicator();
		if(document.getElementById('usuario').value != ""){
			 myApp.confirm('Está preenchido corretamente?',function(){
				$.get(servidorweb2+"recuperacao.php?f=USR&user="+document.getElementById('usuario').value, function(data2, status){
					var splitdado = data2.split(';');
					if(splitdado[0] > 0){
						myApp.hideIndicator();
						myApp.confirm('Olá, '+splitdado[1]+' '+splitdado[2]+' É você?',function(){
							localStorage.setItem('idtrocasenha',splitdado[0]);
							myApp.showIndicator();
							$.get(servidorweb+"notifica/"+splitdado[0]+"/"+splitdado[1]+"/"+splitdado[2]+"/"+splitdado[3]+"/"+splitdado[4]+"/key", function(data2, status){
								myApp.hideIndicator();
								myApp.confirm('Código enviado para o email:'+splitdado[3],function(){
									localStorage.setItem('trocasenhacod',data2);
									localStorage.setItem('trocasenha','1');
									window.location.href='validacodigo.html';
								});
							}).fail(function() {
			             myApp.hideIndicator();
			             myApp.alert('A Conexão falhou! Feche e abra o aplicativo prfvr.'); // or whatever
		          });
						});
					}else{
						myApp.hideIndicator();
						myApp.alert('Nenhum usuário correspondente!');
					}

				}).fail(function() {
					myApp.hideIndicator();
					myApp.alert('A Conexão falhou! Feche e abra o aplicativo prfvr.'); // or whatever
				});
			 });
		}else if(document.getElementById('inputemail').value != ""){
			 myApp.confirm('Está preenchido corretamente?',function(){
				$.get(servidorweb+"recupera/2/"+document.getElementById('inputemail').value+"/key", function(data2, status){
					var splitdado = data2.split(';');
					if(splitdado[0] > 0){
						myApp.hideIndicator();
						myApp.confirm('Olá, '+splitdado[1]+' '+splitdado[2]+' É você?',function(){
							myApp.showIndicator();
							$.get(servidorweb+"notifica/"+splitdado[0]+"/"+splitdado[1]+"/"+splitdado[2]+"/"+splitdado[3]+"/"+splitdado[4]+"/key", function(data2, status){
								myApp.hideIndicator();
								myApp.confirm('Código enviado para o email:'+splitdado[3],function(){
									localStorage.setItem('trocasenhacod',data2);
									localStorage.setItem('trocasenha','1');
									window.location.href='validacodigo.html';
								});
							}).fail(function() {
			myApp.hideIndicator();
			myApp.alert('A Conexão falhou! Feche e abra o aplicativo prfvr.'); // or whatever
		});
						});
					}else{
						myApp.hideIndicator();
						myApp.alert('Nenhum usuário correspondente!');
					}
				}).fail(function() {
			myApp.hideIndicator();
			myApp.alert('A Conexão falhou! Feche e abra o aplicativo prfvr.'); // or whatever
		});
			 });
		}else if(document.getElementById('telefone').value != ""){
			var numsStr = document.getElementById('telefone').value.replace(/[^0-9]/g,'');
			 myApp.confirm('Está preenchido corretamente?',function(){
				$.get(servidorweb+"recupera/3/55"+numsStr+"/key", function(data2, status){
					var splitdado = data2.split(';');
					if(splitdado[0] > 0){
						myApp.hideIndicator();
						myApp.confirm('Olá, '+splitdado[1]+' '+splitdado[2]+' É você?',function(){
							myApp.showIndicator();

							$.get(servidorweb+"notifica/"+splitdado[0]+"/"+splitdado[1]+"/"+splitdado[2]+"/"+splitdado[3]+"/55"+numsStr+"/key", function(data2, status){
								myApp.hideIndicator();
								myApp.confirm('Código enviado para o email:'+splitdado[3],function(){
									localStorage.setItem('trocasenhacod',data2);
									localStorage.setItem('trocasenha','1');
									window.location.href='validacodigo.html';
								});
							}).fail(function() {
			             myApp.hideIndicator();
			             myApp.alert('A Conexão falhou! Feche e abra o aplicativo prfvr.'); // or whatever
		          });
						});
					}else{
						myApp.hideIndicator();
						myApp.alert('Nenhum usuário correspondente!');
					}
				});
			 });
		}else{
			 myApp.hideIndicator();
			myApp.alert('Nenhum campo preenchido!');
		}

	}