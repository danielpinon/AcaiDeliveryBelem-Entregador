// Initialize your app
var myApp = new Framework7({
    animateNavBackIcon:true
});

// Export selectors engine
var $$ = Dom7;

// Add main View
var mainView = myApp.addView('.view-main', {
    // Enable dynamic Navbar
    dynamicNavbar: true,
    // Enable Dom Cache so we can use all inline pages
    domCache: true
});

$$('.open-info').on('click', function () {
  myApp.pickerModal('.picker-info')
});
$$('.close-info').on('click', function () {
  myApp.closeModal('.picker-info')
});      
